#include <iostream>
#include "formageometrica.hpp"

using namespace std;

FormaGeometrica::FormaGeometrica() {
  cout << "#Contrutor da FormaGeometrica" << endl;
  base = 10;
  altura = 20;
  lados = 4;
}
FormaGeometrica::FormaGeometrica(int lados, float base, float altura){
   cout << "#Contrutor da FormaGeometrica" << endl;
   this->base = base;
   this->altura = altura;
   this->lados = lados;
}

// FormaGeometrica::~FormaGeometrica(){
//   std::cout << "Destrutor da FormaGeometrica" << std::endl;
// }

void FormaGeometrica::setBase(float base){
   this->base = base;
}
float FormaGeometrica::getBase(){
   return base;
}
void FormaGeometrica::setAltura(float altura){
  this->altura = altura;
}
float FormaGeometrica::getAltura(){
   return altura;
}
void FormaGeometrica::setLados(int lados){
  this->lados = lados;
}
int FormaGeometrica::getLados(){
   return lados;
}
float FormaGeometrica::getArea(){
   return area;
}
float FormaGeometrica::getPerimetro(){
   return perimetro;
}

void FormaGeometrica::setArea(float area) {
  this->area = area;
}
void FormaGeometrica::setPerimetro(float perimetro){
  this->perimetro = perimetro;
}

/*
void FormaGeometrica::calculaArea(){
   std::cout << "**Calculo de Area da FormaGeometrica" << std::endl;
   area = base * altura;
}
void FormaGeometrica::calculaPerimetro(){
  std::cout << "**Calculo de Perimetro da FormaGeometrica" << std::endl;
   perimetro = 2*base + 2*altura;
}*/
